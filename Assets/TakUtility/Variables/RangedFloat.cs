﻿// ----------------------------------------------------------------------------
// Taken from Unity Development Tools - Ranged Float
// Author: Luis Arzola http://heisarzola.com 
// https://github.com/heisarzola/Unity-Development-Tools/blob/master/Attributes/Ranged%20Float/RangedFloat.cs
// ----------------------------------------------------------------------------

/* Usage example

    [Header("Ranged Float With Locked Limits")]
    [RangedFloat(0, 1, RangedFloatAttribute.RangeDisplayType.LockedRanges)]
    public RangedFloat exampleTwo;

    [Header("Ranged Float With Editable Limits")]
    [RangedFloat(0, 1, RangedFloatAttribute.RangeDisplayType.EditableRanges)]
    public RangedFloat exampleThree;

    [Header("Ranged Float With Hidden (But Locked) Limits")]
    [RangedFloat(0, 1, RangedFloatAttribute.RangeDisplayType.HideRanges)]
    public RangedFloat exampleFour;
*/

using UnityEngine;

namespace TakUtility.Variables
{
    [System.Serializable]
    public class RangedFloat
    {
        [SerializeField] private float min;
        [SerializeField] private float max;

        public float Min { get { return min; } set { min = value; } }
        public float Max { get { return max; } set { max = value; } }


        public void Init()
        {
            min = 0;
            max = 1;
        }

        public RangedFloat()
        {
            min = 0;
            max = 1;
        }

        public RangedFloat(float min, float max)
        {
            this.min = min;
            this.max = max;
        }

        /// <summary>
        /// Gets a random value within the min and max range.
        /// </summary>
        /// <returns></returns>
        public float GetRandomValue()
        {
            return Random.Range(Min, Max);
        }


        public override string ToString()
        {
            return string.Format("[{1}, {2}]", Min, Max);
        }

        /// <summary>
        /// Implicit operator that will automatically fetch a random value within range when a <see cref="RangedFloat"/> is used as a <see cref="float"/>.
        /// </summary>
        /// <param name="someRangedFloat"></param>
        public static implicit operator float(RangedFloat someRangedFloat)
        {
            return someRangedFloat.GetRandomValue();
        }
    }
}