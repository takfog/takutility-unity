﻿// ----------------------------------------------------------------------------
// Taken from Unity Development Tools - Ranged Float
// Author: Luis Arzola http://heisarzola.com 
// https://github.com/heisarzola/Unity-Development-Tools/blob/master/Attributes/Ranged%20Float/RangedFloatAttribute.cs
// ----------------------------------------------------------------------------

using UnityEngine;

/// <summary>
/// An editor use attribute that will allow showing ranged floats with a dual slider.
/// </summary>
[System.Serializable]
public class RangedFloatAttribute : PropertyAttribute
{
    public enum RangeDisplayType
    {
        LockedRanges,
        EditableRanges,
        HideRanges
    }

    public float max;
    public float min;
    public RangeDisplayType rangeDisplayType;

    public RangedFloatAttribute(float min, float max, RangeDisplayType rangeDisplayType = RangeDisplayType.LockedRanges)
    {
        this.min = min;
        this.max = max;
        this.rangeDisplayType = rangeDisplayType;
    }
}
