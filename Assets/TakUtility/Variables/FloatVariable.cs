using UnityEngine;

namespace TakUtility.Variables
{   
    [CreateAssetMenu(menuName="TakUtility/FloatVariable")]
    public class FloatVariable : GenericVariable<float>
    {

        public void ApplyChange(float amount)
        {
            Value += amount;
        }

        public void ApplyChange(FloatVariable amount)
        {
            Value += amount.Value;
        }

        public static implicit operator float(FloatVariable b)
        {
            return b.Value;
        }
    }
}