using System;
using UnityEngine.Events;

namespace TakUtility.Variables
{
    [Serializable]
    public class StringEvent : UnityEvent<string> { }
    [Serializable]
    public class StringVarEvent : UnityEvent<StringVariable> { }

    public class StringVariableListener : VariableListener<string,StringVariable,StringEvent, StringVarEvent>
    {
    }
}