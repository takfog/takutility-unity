using System;
using UnityEngine.Events;

namespace TakUtility.Variables
{
    [Serializable]
    public class FloatEvent : UnityEvent<float> { }
    [Serializable]
    public class FloatVarEvent : UnityEvent<FloatVariable> { }

    public class FloatVariableListener : VariableListener<float,FloatVariable,FloatEvent, FloatVarEvent>
    {

    }
}