// ----------------------------------------------------------------------------
// Taken from Unite 2017 - Game Architecture with Scriptable Objects
// Author: Ryan Hipple
// https://github.com/roboryantron/Unite2017/blob/master/Assets/Code/Variables/FloatVariable.cs
// ----------------------------------------------------------------------------

using UnityEngine;
using TakUtility.Events;

namespace TakUtility.Variables
{
    public abstract class GenericVariable<T> : ScriptableObject
    {
#if UNITY_EDITOR
        [Multiline]
        [SerializeField]
        private string DeveloperDescription = "";
#endif
        [SerializeField]
        private T val;
        [SerializeField]
        private GameEvent changeEvent;
        
        public GameEvent ChangeEvent
        {
            get { return changeEvent; }
        }
        
        public GameEvent CreateEvent() 
        {
            if (changeEvent == null)
                changeEvent = ScriptableObject.CreateInstance<GameEvent>();
            return changeEvent;
        }   
        
        public T Value 
        {
            get { return val; }
            set { SetValue(value); }
        }           

        public void SetValue(T value)
        {
            this.val = value;
            Changed();
        }

        public void SetValue(GenericVariable<T> other)
        {
            this.val = other.val;
            Changed();
        }
        
        protected void Changed()
        {
            if (changeEvent != null)
                changeEvent.Raise();
        }
        
        public override string ToString()
        {
            return val.ToString();
        }

    }
}