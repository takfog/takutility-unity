using UnityEngine;

namespace TakUtility.Variables
{
    [CreateAssetMenu(menuName="TakUtility/StringVariable")]
    public class StringVariable : GenericVariable<string>
    {
        public static implicit operator string(StringVariable b)
        {
            return b.Value;
        }
    }
}