using TakUtility.Events;
using UnityEngine;
using UnityEngine.Events;

namespace TakUtility.Variables
{
    public class VariableListener<T,V,B,E> : MonoBehaviour, IGameEventListener
        where V : GenericVariable<T>
        where B : UnityEvent<T>
        where E : UnityEvent<V>
    {
        [Tooltip("Variable to monitor.")]
        [SerializeField]
        private V variable;

        [Tooltip("Response to invoke when variable is changed (passing the variable value).")]
        [SerializeField]
        private B Response;
        [Tooltip("Response to invoke when variable is changed (passing the variable itself).")]
        [SerializeField]
        private E ResponseWithObject;

        private void OnEnable()
        {
            variable.CreateEvent();
            variable.ChangeEvent.RegisterListener(this);
        }

        private void OnDisable()
        {
            variable.ChangeEvent.UnregisterListener(this);
        }

        public void OnEventRaised()
        {
            Response.Invoke(variable.Value);
            ResponseWithObject.Invoke(variable);
        }
    }
}