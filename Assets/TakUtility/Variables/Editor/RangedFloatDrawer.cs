﻿// ----------------------------------------------------------------------------
// Taken from Unity Development Tools - Ranged Float
// Author: Luis Arzola http://heisarzola.com 
// https://github.com/heisarzola/Unity-Development-Tools/blob/master/Attributes/Ranged%20Float/Editor/RangedFloatDrawer.cs
// ----------------------------------------------------------------------------

using UnityEditor;
using UnityEngine;

namespace TakUtility.Variables
{
    /// <summary>
    /// Fallback drawer in case the class RangedFloat is inteneded to be used without the RangedFloatAttribute.
    /// </summary>
    [CustomPropertyDrawer(typeof(RangedFloat))]
    public class RangedFloatDrawer : PropertyDrawer
    {
        private const int _AMOUNT_OF_ITEMS = 1;
        private readonly float _spacerHeight = 20f;
        private readonly float _lineHeight = 16f;
        private string _name = string.Empty;
        private string _tooltip = string.Empty;
        private bool _cache = false;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return _AMOUNT_OF_ITEMS * _spacerHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUIUtility.labelWidth /= 4f;
            position.height = _lineHeight;
            position.width /= 4f;

            if (!_cache)
            {
                //get the name before it's gone
                _name = property.displayName;
                _tooltip = property.tooltip;

                _cache = true;
            }

            EditorGUI.PrefixLabel(position, new GUIContent(_name,
                string.Format("Base Tooltip: {0}", _tooltip.Equals(string.Empty) ? "" : string.Format("\n\n{0}'s Tooltip:\n{1}", _name, _tooltip))));

            position.x += position.width;

            position.width *= 4f;
            position.width *= 0.375f;

            EditorGUI.PropertyField(position, property.FindPropertyRelative("min"),
                new GUIContent("Min"));

            position.x += position.width;

            EditorGUI.PropertyField(position, property.FindPropertyRelative("max"),
                new GUIContent("Max"));

        }
    }
}
