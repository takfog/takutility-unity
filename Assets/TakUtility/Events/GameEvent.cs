// ----------------------------------------------------------------------------
// Taken from Unite 2017 - Game Architecture with Scriptable Objects
// Author: Ryan Hipple
// https://github.com/roboryantron/Unite2017/blob/master/Assets/Code/Events/GameEvent.cs
// ----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using UnityEngine;

namespace TakUtility.Events
{
    public interface IGameEventListener
    {
        void OnEventRaised();
    }

    [CreateAssetMenu(menuName="TakUtility/GameEvent")]
    public class GameEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<IGameEventListener> eventListeners = 
            new List<IGameEventListener>();

        public event Action Changed;

        public void Raise()
        {
            for(int i = eventListeners.Count -1; i >= 0; i--)
                eventListeners[i].OnEventRaised();
            if (Changed != null) Changed();
        }

        public void RegisterListener(IGameEventListener listener)
        {
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }

        public void UnregisterListener(IGameEventListener listener)
        {
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }
    }
}