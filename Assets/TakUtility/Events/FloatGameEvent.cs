﻿using UnityEngine;

namespace TakUtility.Events
{
    [CreateAssetMenu(menuName = "TakUtility/Float GameEvent")]
    public class FloatGameEvent : ArgGameEvent<float>
    {
    }
}