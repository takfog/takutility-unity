﻿using System.Collections.Generic;
using UnityEngine;

namespace TakUtility.Events
{
    public interface IArgGameEventListener<T>
    {
        void OnEventRaised(T value);
    }

    public class ArgGameEvent<T> : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<IArgGameEventListener<T>> eventListeners =
            new List<IArgGameEventListener<T>>();

        public void Raise(T value)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(value);
        }

        public void RegisterListener(IArgGameEventListener<T> listener)
        {
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }

        public void UnregisterListener(IArgGameEventListener<T> listener)
        {
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }
    }
}