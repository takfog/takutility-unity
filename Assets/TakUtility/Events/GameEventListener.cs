// ----------------------------------------------------------------------------
// Taken from Unite 2017 - Game Architecture with Scriptable Objects
// Author: Ryan Hipple
// https://github.com/roboryantron/Unite2017/blob/master/Assets/Code/Events/GameEventListener.cs
// ----------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.Events;

namespace TakUtility.Events
{
    public class GameEventListener : MonoBehaviour, IGameEventListener
    {
        [Tooltip("Event to register with.")]
        [SerializeField]
        private GameEvent Event;

        [Tooltip("Response to invoke when Event is raised.")]
        [SerializeField]
        private UnityEvent Response;

        private void OnEnable()
        {
            Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            Event.UnregisterListener(this);
        }

        public void OnEventRaised()
        {
            Response.Invoke();
        }
    }
}