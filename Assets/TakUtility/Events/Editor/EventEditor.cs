// ----------------------------------------------------------------------------
// Taken from Unite 2017 - Game Architecture with Scriptable Objects
// Author: Ryan Hipple
// https://github.com/roboryantron/Unite2017/blob/master/Assets/Code/Events/Editor/EventEditor.cs
// ----------------------------------------------------------------------------

using UnityEditor;
using UnityEngine;

namespace TakUtility.Events
{
    [CustomEditor(typeof(GameEvent))]
    public class EventEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            GameEvent e = target as GameEvent;
            if (GUILayout.Button("Raise"))
                e.Raise();
        }
    }
}