using UnityEngine;

namespace TakUtility
{
    [RequireComponent(typeof(AudioSource))]
    public class MusicLoopStarter : MonoBehaviour {
        [SerializeField]
        private AudioClip loop;
        
        private AudioSource src;

        void Start() 
        {
            src = GetComponent(typeof(AudioSource)) as AudioSource;
        }
        
        void Update() 
        {
            if (!src.isPlaying)
            {
                src.clip = loop;
                src.loop = true;
                src.Play();
                this.enabled = false;
            }
        }
    }
}