using UnityEngine;
using TakUtility.Variables;

namespace TakUtility
{
    public class Shake : MonoBehaviour {
        [SerializeField]
        private FloatVariable power;
        [SerializeField]
        private FloatVariable fadeFactor;
        
        private float remainingPower;
        private Vector3 startPosition;
        
        void OnEnable()
        {
            remainingPower = power.Value;
            startPosition = transform.localPosition;
        }
        
        void Update () 
        {
            remainingPower *= fadeFactor.Value;
            if (remainingPower < 0.01f)
            {
                transform.localPosition = startPosition;
                enabled = false;
                return;
            }
            
            Vector2 shake = Random.insideUnitCircle * remainingPower;
            transform.localPosition = new Vector3(startPosition.x + shake.x, startPosition.y + shake.y, startPosition.z);
        }
    }
}
